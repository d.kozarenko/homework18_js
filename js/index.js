"use strict";

const object = {
    secondObject: {
        name: "bib",
        lastname: "bub",
        thirdObject: {
            warning: "third level",
        },
    },
    skills: ["JS", "html", "css"],
}
function recursiveCloning(object) {
    if (typeof object !== "object") {
        return object;
    } else if (Array.isArray(object)) {
        return object;
    } else if (object === null) {
        return object;
    } else {
        let result = {};
        for (let key in object) {
            result[key] = recursiveCloning(object[key]);
        }
        return result;
    }
};
console.log(recursiveCloning(object));
